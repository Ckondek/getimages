#! /bin/usr/python3



import requests
import pprint

pp = pprint.PrettyPrinter(indent=4)

r = requests.get('https://tangled.garden/api/images/?format=json')
jsonGarden=r.json()
#pp.pprint(jsonGarden)
print("\n------------------------")

#--------------if I already know which image i want i can parse the json no problem.

seed1=jsonGarden[0]
pp.pprint(seed1)
uuid=seed1["uuid"]
generation=seed1["generation"]

print("Picture with UUID: {} is generation {}".format(uuid,generation))
print("\n-------------------")


#---But how can I get only images from generation 3 for example
#--I want ot be able to do something like
# gen3list=requests.get('https://tangled.garden/api/images/?generation=3')

#we dont have any generation 3 pictures so it is hard to test but I can try to grab an image by uuid

requestUUID=requests.get('https://tangled.garden/api/images/?uuid={}'.format(uuid))

print(requestUUID.url)

#this doesnt work. Instead of just printing the item with the correct UUID it prints out the whole json file.


pp.pprint(requestUUID.json())
print("----------------")

#how can I just grab what I want from the database?

#because I couldnt access a single item I couldn't figure out how to update
#an entry. zB. Change an item so the children field is updated.


#because I couldnt access a single item I couldn't figure out how to update
#an entry. zB. Change an item so the children field is updated.

#if I want to update an entry with 'uuid' uuid to generaion 3  I tried:

payload = {'uuid': uuid, 'generation': 3}
r = requests.post("https://httpbin.org/post", data=payload)
requests.post

print(r.text)

#not working

